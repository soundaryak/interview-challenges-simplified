/**
 * Test 2
 * Choose one of the following
 */

/**
 * Fix the function so that it returns [4,[1],[2],[3],[4]];
 */

/* solution: created an array table of size 4,
   * pushing elements 1 to 4
   * used different variable name in for loop to avoid confusion.
 */

var count = 4;
function foo() {
  var table = [4];
  while (count-- > 0) {
    table.push([]);
  }
  for (var i = 1; i < table.length; i++) {
    table[i].push(i);
  }
  return table;
}

console.log(foo());



////////
// OR //
////////

/*
 * The desired result is a countdown from 5 to 0 using console log messages. 
 * Explain why the code only logs -1, then fix the code so it works as expected.
 */
/* Notes for solution: due to setTimeout function, the function is not stepping out from current function,
 *due to that log just displaying -1. The setTimeout will be executed only if it comes out from loop.
*/

var countdown = function(num){
    for (var i = 0; i <= num; i += 1) {
        (function(i){
          setTimeout(function(){
          console.log(num - i);
          }, i * 1000);
        })(i);
    }
}
countdown(5);

